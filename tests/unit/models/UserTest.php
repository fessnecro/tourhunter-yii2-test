<?php

namespace tests\models;

use app\models\User;

class UserTest extends \Codeception\Test\Unit
{
    public function testFindUserById()
    {
        expect_that($user = User::findIdentity(1));
        expect_that($user = User::findIdentity(1));
    }


    public function testValidateUser()
    {
        $user = User::getUser('test');
        expect_that($user->validateAuthKey('authtest'));
        expect_not($user->validateAuthKey('authtest2'));;
    }

    public function testValidateUserBalance()
    {
        $users = User::find()->where(['<', 'balance', User::MIN_BALANCE])->all();
        expect($users)->isEmpty();
    }

}
