<?php

namespace tests\models;

use app\models\LoginForm;

class LoginFormTest extends \Codeception\Test\Unit
{
    private $model;

    protected function _after()
    {
        \Yii::$app->user->logout();
    }

    public function testLoginNoUser()
    {
        $this->model = new LoginForm([
            'login' => 'VERY LONG LOGIN ...................................>!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!',
        ]);
        expect_not($this->model->login());
        expect_that(\Yii::$app->user->isGuest);
    }


    public function testLoginCorrect()
    {
        $this->model = new LoginForm([
            'login' => 'test',
        ]);

        expect_that($this->model->login());
        expect_not(\Yii::$app->user->isGuest);
    }

}