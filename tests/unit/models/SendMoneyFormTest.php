<?php

namespace tests\models;

use app\models\User;
use app\models\LoginForm;
use app\models\SendMoneyForm;

class SendMoneyFormTest extends \Codeception\Test\Unit
{
    private $model;
    private $amount = 100;
    private $toLogin = 'test2';

    protected function _after()
    {
        \Yii::$app->user->logout();
    }

    public function testSendMoney()
    {
        $loginForm = new LoginForm([
            'login' => 'test',
        ]);

        expect_that($loginForm->login());
        expect_not(\Yii::$app->user->isGuest);

        $user = \Yii::$app->user->identity;

        expect_that(\Yii::$app->user->identity->login != $this->toLogin);

        $this->model = new SendMoneyForm([
            'amount' => $this->amount,
            'toLogin' => $this->toLogin,
        ]);


        if ($user->balance - $this->amount >= User::MIN_BALANCE) {
            expect_that($this->model->send());
        } else {
            expect_not($this->model->send());
        }


    }

}


