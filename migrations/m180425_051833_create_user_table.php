<?php

use yii\db\Migration;
use app\models\User;
/**
 * Handles the creation of table `user`.
 */
class m180425_051833_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'login' => $this->string(20)->notNull(),
            'balance' => $this->float()->notNull()->defaultValue(0),
            'auth_key' => $this->string(32)->notNull(),
            'created_at' => $this->timestamp()->notNull()
        ]);

        $testUser = new User;
        $testUser->login = 'test';
        $testUser->auth_key = 'authtest';
        $testUser->save();

        $secondTestUser = new User;
        $secondTestUser->login = 'test2';
        $secondTestUser->auth_key = 'authtest2';
        $secondTestUser->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user');
    }
}
