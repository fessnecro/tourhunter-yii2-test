<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Transaction */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Transactions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transaction-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' =>  'from',
                'value' => function ($model) {
                    return Html::a($model->fromUser->login, ['user/view', 'id' => $model->fromUser->id]);
                },
                'format' => 'html'
            ],
            [
                'attribute' =>  'to',
                'value' => function ($model) {
                    return Html::a($model->toUser->login, ['user/view', 'id' => $model->toUser->id]);
                },
                'format' => 'html'
            ],
            'amount',
            'created_at',
        ],
    ]) ?>

</div>
