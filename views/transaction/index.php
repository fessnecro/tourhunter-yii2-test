<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Transactions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transaction-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function($model) {
            if ($model->from == Yii::$app->user->identity->id) {
                $class = 'danger';
            } else {
                $class = 'success';
            }
            return ['class' => $class];
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' =>  'from',
                'value' => function ($model) {
                    return Html::a($model->fromUser->login, ['user/view', 'id' => $model->fromUser->id]);
                },
                'format' => 'html'
            ],
            [
                'attribute' =>  'to',
                'value' => function ($model) {
                    return Html::a($model->toUser->login, ['user/view', 'id' => $model->toUser->id]);
                },
                'format' => 'html'
            ],
            'amount',
            'created_at',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}'
            ],
        ],
    ]); ?>
</div>
