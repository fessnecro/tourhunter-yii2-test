<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\SendMoneyForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use app\models\User;

$this->title = 'Send Money';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php if ($success): ?>
        <?php Alert::begin([
            'options' => [
            'class' => 'alert-success',
            ],
        ]);?>

        <?= 'You have success send money'; ?>

        <?php Alert::end(); ?>
    <?php endif; ?>

    <p>Please fill out the following fields to send money:</p>

    <?php $form = ActiveForm::begin([
        'id' => 'send-money-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

    <?php Alert::begin([
        'options' => [
            'class' => 'alert-success',
        ],
    ]);?>

    <?= 'You can send up to ' . (Yii::$app->user->identity->balance - User::MIN_BALANCE) . ' amount of money'; ?>

    <?php Alert::end(); ?>

    <?= $form->field($model, 'amount')->textInput(['autofocus' => true]) ?>

    <?= $form->field($model, 'toLogin')->textInput(['autofocus' => true]) ?>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
