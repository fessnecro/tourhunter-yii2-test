<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $login
 * @property double $balance
 * @property string $auth_key
 * @property string $created_at
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    const MIN_BALANCE = -1000;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login', 'auth_key'], 'required'],
            [['balance'], 'number', 'min' => self::MIN_BALANCE],
            [['created_at'], 'safe'],
            [['login'], 'string', 'max' => 20],
            [['auth_key'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Login',
            'balance' => 'Balance',
            'auth_key' => 'Auth Key',
            'created_at' => 'Created At',
        ];
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new Exception('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */

    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }

    /**
     * Get or register new user
     * @param string $login
     * @return User
     */
    public static function getUser($login)
    {
        if (!$user = User::findOne(['login' => $login])) {
            $user = new User;
            $user->login = $login;
            $user->generateAuthKey();
            if (!$user->save()) {
                var_dump($user->getErrors());
                throw new \Exception('Something went wrong');
            }
        }
        return $user;
    }
}
