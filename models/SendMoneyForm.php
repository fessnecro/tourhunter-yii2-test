<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\Exception;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class SendMoneyForm extends Model
{
    public $amount;
    public $toLogin;



    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['amount', 'toLogin'], 'required'],
            [['toLogin'], 'string', 'max' => 20],
            [['amount'], 'number'],
        ];
    }


    /**
     * Send money to $this->toLogin user login
     * If no user in db, create it with login $this->toLogin
     * @return boolean.
     */
    public function send()
    {
        $dbTransaction = Yii::$app->db->beginTransaction();

        $user = Yii::$app->user->identity;

        if ($user->login == $this->toLogin) {
            $this->addError('toLogin', 'You cant send money to yourself');
            return false;
        }

        if ($this->amount <= 0) {
            $this->addError('amount', 'Only posotive value');
            return false;
        }

        if ($user->balance - $this->amount >= User::MIN_BALANCE) {
            $user->balance -= $this->amount;
            $toUser = User::getUser($this->toLogin);
            $toUser->balance += $this->amount;
            $transaction = new Transaction;
            $transaction->amount = $this->amount;
            $transaction->from = $user->id;
            $transaction->to = $toUser->id;
            if ($user->save() && $toUser->save() && $transaction->save()) {
                $dbTransaction->commit();
                return true;
            } else {
                $dbTransaction->rollBack();
                throw new \Exception('Something went wrong');
            }

        } else {
            $this->addError('amount', 'You\'r balance is too low, pls donate tourhunter to gain more balance');
        }
        return false;
    }
}
