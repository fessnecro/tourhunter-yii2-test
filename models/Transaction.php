<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "transaction".
 *
 * @property int $id
 * @property int $from
 * @property int $to
 * @property int $amount
 * @property string $created_at
 */
class Transaction extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transaction';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from', 'to'], 'integer'],
            [['amount'], 'number'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'from' => 'From',
            'to' => 'To',
            'amount' => 'Amount',
            'created_at' => 'Created At',
        ];
    }

    public function getToUser()
    {
        return $this->hasOne(User::class, ['id' => 'to'])->from(User::tableName() . ' toUser');;
    }

    public function getFromUser()
    {
        return $this->hasOne(User::class, ['id' => 'from'])->from(User::tableName() . ' fromUser');;
    }
}
